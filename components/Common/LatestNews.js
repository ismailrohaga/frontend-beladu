import React from "react";
import Link from "next/link";
import { useBlog } from "hooks";
import BlogCard from "components/Card/BlogCard";

const LatestNews = () => {
  const { useGetBlogList } = useBlog();
  const { data: results, isLoading } = useGetBlogList(["getBlogList"]);

  return (
    <div className="blog-area ptb-100">
      <div className="container">
        <div className="section-title">
          <h2>Update Terbaru Beladu</h2>
          <p>Lihat bagaimana Beladu berkontribusi membantu UMKM</p>
        </div>

        <div className="row">
          {isLoading ? (
            <h4 className="text-center">Mengambil Data Blog...</h4>
          ) : results.data.length >= 3 ? (
            results.data
              .slice(0, 3)
              .map((blog, key) => <BlogCard key={key} {...blog} />)
          ) : (
            results.data.map((blog, key) => <BlogCard key={key} {...blog} />)
          )}

          <div className="col-lg-12 col-md-12">
            <div className="blog-post-info">
              <p>
                Tidak menemukan yang anda cari?​{" "}
                <Link href="/blog-1">
                  <a>Lihat semua post</a>
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LatestNews;
