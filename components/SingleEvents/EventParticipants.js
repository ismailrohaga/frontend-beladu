import React from "react";
import Link from "next/link";
import dynamic from "next/dynamic";
const OwlCarousel = dynamic(import("react-owl-carousel3"));

const options = {
  loop: false,
  nav: false,
  dots: true,
  autoplayHoverPause: true,
  autoplay: true,
  margin: 30,
  navText: [
    "<i class='bx bx-chevron-left'></i>",
    "<i class='bx bx-chevron-right'></i>",
  ],
  responsive: {
    0: {
      items: 1,
    },
    576: {
      items: 2,
    },
    768: {
      items: 2,
    },
    992: {
      items: 3,
    },
  },
};

const EventSpeakers = ({ speakers }) => {
  const [display, setDisplay] = React.useState(false);

  React.useEffect(() => {
    setDisplay(true);
  }, []);
  return (
    <div className="advisor-area bg-f9f9f9 ptb-100">
      <div className="container">
        <div className="section-title">
          <h2>Pembicara</h2>
        </div>

        {display ? (
          <OwlCarousel
            className="advisor-slides-two owl-carousel owl-theme"
            {...options}
          >
            {speakers?.map((speaker, key) => (
              <div className="single-advisor-item" key={key}>
                <div className="advisor-image">
                  <img src={speaker.speakerPhotoUrl} alt="image" />
                </div>

                <div className="advisor-content">
                  <h3>
                    <Link href="#">
                      <a>{speaker.speakerName}</a>
                    </Link>
                  </h3>
                  <span>
                    {speaker.speakerAgency} - {speaker.speakerRole}
                  </span>
                </div>
              </div>
            ))}
          </OwlCarousel>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default EventSpeakers;
