import React, { useState } from "react";
import Link from "next/link";
import ModalEventBookingForm from "components/Modal/ModalEventBookingForm";

const EventsSidebar = ({ data }) => {
  const [showModal, setShowModal] = useState(false);

  let isShowRegisterButton = true;

  return (
    <div className="events-details-info">
      <ul className="info">
        {/* <li className="price">
          <div className="d-flex justify-content-between align-items-center">
            <span>Cost</span>
            $149
          </div>
        </li> */}
        <li>
          <div className="d-flex justify-content-between align-items-center">
            <span>Kuota Tersedia</span>
            {data?.participantLimit}
          </div>
        </li>
        <li>
          <div className="d-flex justify-content-between align-items-center">
            <span>Jumlah Pendaftar</span>
            {data?._count?.userEvent}
          </div>
        </li>
        {/* <li>
          <div className="d-flex justify-content-between align-items-center">
            <span>Pay With</span>
            <div className="payment-method">
              <img
                src="/images/payment/payment1.png"
                className="shadow"
                alt="image"
              />
              <img
                src="/images/payment/payment2.png"
                className="shadow"
                alt="image"
              />
              <img
                src="/images/payment/payment3.png"
                className="shadow"
                alt="image"
              />
              <img
                src="/images/payment/payment4.png"
                className="shadow"
                alt="image"
              />
            </div>
          </div>
        </li> */}
      </ul>

      <div className="btn-box">
        <a className="default-btn" onClick={() => setShowModal(true)}>
          Daftar Event <span></span>
        </a>
      </div>

      {/* <div className="events-share">
        <div className="share-info">
          <span>
            Bagikan Event Ini <i className="flaticon-share"></i>
          </span>
        </div>
      </div> */}

      <ModalEventBookingForm
        open={showModal}
        onClose={() => setShowModal(false)}
      />
    </div>
  );
};

export default EventsSidebar;
