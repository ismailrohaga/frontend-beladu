import React from "react";
import Head from "next/head";
import GoTop from "./GoTop";

const Layout = ({ children }) => {
  return (
    <React.Fragment>
      <Head>
        <title>BELADU - Belajar Langsung Dapat Uang</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta
          name="description"
          content="BELADU - Belajar Langsung Dapat Uang"
        />
        <meta
          name="og:title"
          property="og:title"
          content="BELADU - Belajar Langsung Dapat Uang"
        ></meta>
        <meta
          name="twitter:card"
          content="BELADU - Belajar Langsung Dapat Uang"
        ></meta>
        <link rel="canonical" href="https://beladu.com/"></link>
      </Head>

      {children}

      <GoTop scrollStepInPx="100" delayInMs="10.50" />
    </React.Fragment>
  );
};

export default Layout;
