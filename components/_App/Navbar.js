import React from "react";
import Link from "../../utils/ActiveLink";

const Navbar = () => {
  const [menu, setMenu] = React.useState(true);

  const toggleNavbar = () => {
    setMenu(!menu);
  };

  React.useEffect(() => {
    let elementId = document.getElementById("navbar");
    document.addEventListener("scroll", () => {
      if (window.scrollY > 170) {
        elementId.classList.add("is-sticky");
      } else {
        elementId.classList.remove("is-sticky");
      }
    });
    window.scrollTo(0, 0);
  });

  const classOne = menu
    ? "collapse navbar-collapse"
    : "collapse navbar-collapse show";
  const classTwo = menu
    ? "navbar-toggler navbar-toggler-right collapsed"
    : "navbar-toggler navbar-toggler-right";

  return (
    <React.Fragment>
      <div id="navbar" className="navbar-area">
        <div className="edemy-nav">
          <div className="container-fluid">
            <div className="navbar navbar-expand-lg navbar-dark bg-black">
              <Link href="/">
                <a onClick={toggleNavbar} className="navbar-brand">
                  <img src="/images/logo2.png" alt="logo" />
                </a>
              </Link>

              <button
                onClick={toggleNavbar}
                className={classTwo}
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="icon-bar top-bar"></span>
                <span className="icon-bar middle-bar"></span>
                <span className="icon-bar bottom-bar"></span>
              </button>

              <div className={classOne} id="navbarSupportedContent">
                <form className="search-box">
                  <input
                    type="text"
                    className="input-search"
                    placeholder="Search for anything"
                  />
                  <button type="submit">
                    <i className="flaticon-search"></i>
                  </button>
                </form>

                <ul className="navbar-nav">
                  <li className="nav-item">
                    <Link href="/#" activeClassName="active">
                      <a onClick={toggleNavbar} className="nav-link">
                        Home
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link href="#" activeClassName="active">
                      <a
                        onClick={(e) => e.preventDefault()}
                        className="nav-link"
                      >
                        Kategori
                      </a>
                    </Link>

                    <ul className="dropdown-menu">
                      <li className="nav-item">
                        <Link href="/coming-soon" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Peternakan
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-2" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Perkebunan
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-3" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Kuliner
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-4" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Kerajinan
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-5" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Audio & Visual
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-6" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Pemasaran
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-7" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Kampung Kreatif
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-7" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            Musik Tradisional
                          </a>
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link href="/index-8" activeClassName="active">
                          <a onClick={toggleNavbar} className="nav-link">
                            IT & Software <i className="bx bx-chevron-down"></i>
                          </a>
                        </Link>

                        <ul className="dropdown-menu">
                          <li className="nav-item">
                            <Link href="/about-1" activeClassName="active">
                              <a onClick={toggleNavbar} className="nav-link">
                                Sub Category 01
                              </a>
                            </Link>
                          </li>

                          <li className="nav-item">
                            <Link href="/about-2" activeClassName="active">
                              <a onClick={toggleNavbar} className="nav-link">
                                Sub Category 02
                              </a>
                            </Link>
                          </li>

                          <li className="nav-item">
                            <Link href="/about-3" activeClassName="active">
                              <a onClick={toggleNavbar} className="nav-link">
                                Sub Category 03
                              </a>
                            </Link>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item">
                    <Link href="/events" activeClassName="active">
                      <a onClick={toggleNavbar} className="nav-link">
                        Event
                      </a>
                    </Link>
                  </li>

                  <li className="nav-item megamenu">
                    <Link href="/coming-soon" activeClassName="active">
                      <a onClick={toggleNavbar} className="nav-link">
                        Konsultasi
                      </a>
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link href="/blog-4" activeClassName="active">
                      <a onClick={toggleNavbar} className="nav-link">
                        Blog
                      </a>
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link href="/coming-soon" activeClassName="active">
                      <a onClick={toggleNavbar} className="nav-link">
                        Jadi Pengajar
                      </a>
                    </Link>
                  </li>
                </ul>

                {/* TODO: ENABLE ONCE AUTH IS READY */}
                {/* <div className="others-option d-flex align-items-center">
                  <div className="option-item">
                    <div className="cart-btn">
                      <Link href="/coming-soon">
                        <a>
                          <i className="flaticon-shopping-cart"></i>{" "}
                          <span>3</span>
                        </a>
                      </Link>
                    </div>
                  </div>

                  <div className="option-item">
                    <Link href="/profile-authentication">
                      <a className="default-btn">
                        Login/Register <span></span>
                      </a>
                    </Link>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Navbar;
