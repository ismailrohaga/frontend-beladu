import React from "react";
import Link from "next/link";

const MainBanner = () => {
  return (
    <div className="banner-section">
      <div className="container-fluid">
        <div className="row align-items-center">
          <div className="col-lg-5 col-md-12">
            <div className="banner-content">
              <h1>
                Mau Belajar, Langsung Buka Usaha, Dapat Akses Pasar dan Cuan?{" "}
              </h1>
              <p className="text-light">
                Solusinya adalah di BELADU/BELADU.COM karena kita menyediakan
                modul-modul pembelajaran terkemuka dan juga layanan konsultasi
                selama masa pembelajaran, agar bisnis anda dapat berkembang dari
                zero to hero. Tunggu apa lagi? Ayo daftar Sekarang!
              </p>

              <Link href="/profile-authentication">
                <a className="default-btn">
                  {" "}
                  Daftar Sekarang <span></span>
                </a>
              </Link>
            </div>
          </div>

          <div className="col-lg-7 col-md-12">
            <div className="banner-image">
              <img src="/images/banner.png" alt="image" />

              <div className="banner-shape4">
                <img src="/images/banner-shape4.png" alt="image" />
              </div>
              <div className="banner-shape5">
                <img src="/images/banner-shape5.png" alt="image" />
              </div>
              <div className="banner-shape6">
                <img src="/images/banner-shape6.png" alt="image" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainBanner;
