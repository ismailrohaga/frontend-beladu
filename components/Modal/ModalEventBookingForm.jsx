import { useEvent } from "hooks";
import { useRouter } from "next/router";
import { useState } from "react";

const ModalEventBookingForm = ({ open, onClose }) => {
    const { query } = useRouter();
    const { useBookingEvent } = useEvent();
    const {
        mutate: mutateBookingEvent,
        isLoading,
        isError,
        error,
    } = useBookingEvent();

    const [formData, setFormData] = useState({
        eventId: query?.id,
    });

    const handleFormChange = (name, value) => {
        setFormData({ ...formData, [name]: value });
    };

    const onSubmitEventBooking = async (event) => {
        event.preventDefault();

        mutateBookingEvent(formData, {
            onSuccess: () => onClose(),
        });
    };

    return (
        <div
            id="open-modal"
            className={`custom-modal ${open ? "show-modal" : ""}`}
        >
            <div className="custom-modal-content">
                <span onClick={onClose} className="close-button">
                    &times;
                </span>
                <div className="register-event-form">
                    <h2>Event Booking Form</h2>
                    {isError && <p className="text-danger">{error}</p>}

                    <form onSubmit={onSubmitEventBooking}>
                        <div className="form-group">
                            <label>Nama Lengkap</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nama"
                                onChange={(e) =>
                                    handleFormChange("userName", e.target.value)
                                }
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label>No Telp</label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="No Telp"
                                onChange={(e) =>
                                    handleFormChange(
                                        "userPhone",
                                        e.target.value
                                    )
                                }
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label>Email</label>
                            <input
                                type="email"
                                className="form-control"
                                placeholder="Email"
                                onChange={(e) =>
                                    handleFormChange(
                                        "userEmail",
                                        e.target.value
                                    )
                                }
                                required
                            />
                        </div>

                        <button type="submit" disabled={isLoading}>
                            Booking
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ModalEventBookingForm;
