import Link from "next/link";
import React from "react";
import { generateIndonesianDate } from "utils/intl";

const BlogCard = ({
    author,
    category,
    coverImageUrl,
    content,
    createdAt,
    id,
    name,
}) => {
    const link = `/blogs/${id}`;

    return (
        <div className="col-lg-4 col-md-6">
            <div className="single-courses-box">
              <div className="courses-image">
                <Link href={link}>
                  <a className="d-block image">
                    <img src={coverImageUrl} alt="image" />
                  </a>
                </Link>
              </div>

              <div className="courses-content">
                <div className="row mb-2">
                  <h6 className="col">
                    <b>{category.name}</b>
                  </h6>
                  <p className="col text-end">{generateIndonesianDate(createdAt)}</p>
                </div>

                <div className="course-author d-flex align-items-center">
                  <img
                    src={author.profileImageUrl}
                    className="rounded-circle"
                    alt="image"
                  />
                  <span>{author.name}</span>
                </div>

                <h3 className="text-truncate">
                  <Link href={link}>
                    <a>
                    {name}
                    </a>
                  </Link>
                </h3>

                <p className="text-truncate">
                {content}
                </p>
              </div>
            </div>
        </div>
    );
};

export default BlogCard;
