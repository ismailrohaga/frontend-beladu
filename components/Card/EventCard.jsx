import Link from "next/link";
import React from "react";
import { generateIDR, generateIndonesianDate } from "utils/intl";

const EventCard = ({
    category,
    coverImageUrl,
    content,
    dateTime,
    id,
    location,
    name,
    price,
}) => {
    const detailEventLink = `/events/${id}`;

    return (
        <div className="col-lg-4 col-md-6">
            <div className="single-courses-box">
                <div className="courses-image">
                    <Link href={detailEventLink}>
                        <a className="d-block image">
                            <img src={coverImageUrl} alt="image" />
                        </a>
                    </Link>
                </div>

                <div className="courses-content">
                    <div className="row mb-2">
                        <h6 className="col">
                            <b>{category.name}</b>
                        </h6>
                        <p className="col text-end">
                            {generateIndonesianDate(dateTime)}
                        </p>
                    </div>

                    <h3 className="text-truncate">
                        <Link href={detailEventLink}>
                            <a>{name}</a>
                        </Link>
                    </h3>

                    <p className="text-truncate">{content}</p>

                    {/* <h6>
                        <b>{generateIDR(price)}</b>
                    </h6> */}

                    {/* TODO: add use geocode val */}
                    {/* <span className="location">
                        <i className="bx bx-map"></i> {location}
                    </span> */}
                </div>
            </div>
        </div>
    );
};

export default EventCard;
