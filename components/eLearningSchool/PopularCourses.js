import React from "react";
import Link from "next/link";

const PopularCourses = () => {
  return (
    <div className="courses-area ptb-100">
      <div className="container">
        <div className="section-title">
          <h2>Video Pembelajaran Terbaik</h2>
          <p>
            Jelajahi semua pembelajaran dan pilih sesuai dengan yang anda suka!
          </p>
        </div>

        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="single-courses-box">
              <div className="courses-image">
                <Link href="/single-courses-1">
                  <a className="d-block image">
                    <img src="/images/courses/courses1.png" alt="image" />
                  </a>
                </Link>
              </div>

              <div className="courses-content">
                <div className="row mb-2">
                  <h6 className="col">
                    <b>Pemasaran</b>
                  </h6>
                  <p className="col text-end">Semua Tingkat</p>
                </div>
                <div className="course-author d-flex align-items-center">
                  <img
                    src="/images/user1.jpg"
                    className="rounded-circle"
                    alt="image"
                  />
                  <span>Karen Julia</span>
                </div>

                <h3>
                  {/* <Link href="/single-courses-1"> */}
                  <Link href="/#">
                    <a>
                      Berjualan Online dengan Strategi Social Media Marketing
                    </a>
                  </Link>
                </h3>

                <p>
                  Pengenalan dan Pemanfaatan Social Media Marketing untuk
                  Keperluan Bisnis/ Usaha.
                </p>

                {/* TODO: Add star rating component */}

                {/* <h6>
                  <b>Rp10.000</b>
                </h6> */}
              </div>
            </div>
          </div>

          {/* Budidaya */}
          <div className="col-lg-4 col-md-6">
            <div className="single-courses-box">
              <div className="courses-image">
                <Link href="/single-courses-1">
                  <a className="d-block image">
                    <img src="/images/courses/courses2.png" alt="image" />
                  </a>
                </Link>
              </div>

              <div className="courses-content">
                <div className="row mb-2">
                  <h6 className="col">
                    <b>Perkebunan</b>
                  </h6>
                  <p className="col text-end">Semua Tingkat</p>
                </div>
                <div className="course-author d-flex align-items-center">
                  <img
                    src="/images/user1.jpg"
                    className="rounded-circle"
                    alt="image"
                  />
                  <span>Adi Hasan</span>
                </div>

                <h3>
                  <Link href="/#">
                    <a>Budidaya Tanaman Dengan Teknologi IoT</a>
                  </Link>
                </h3>

                <p>
                  Apa itu IoT? Bagaimana cara seorang petani dapat memanfaatkan
                  IoT? Seperti apa strategi dan langkah yang perlu dipersiapkan?
                </p>

                {/* TODO: Add star rating component */}

                {/* <h6>
                  <b>Rp10.000</b>
                </h6> */}
              </div>
            </div>
          </div>

          {/* Makanan */}
          <div className="col-lg-4 col-md-6">
            <div className="single-courses-box">
              <div className="courses-image">
                <Link href="/single-courses-1">
                  <a className="d-block image">
                    <img src="/images/courses/courses3.png" alt="image" />
                  </a>
                </Link>
              </div>

              <div className="courses-content">
                <div className="row mb-2">
                  <h6 className="col">
                    <b>Makanan</b>
                  </h6>
                  <p className="col text-end">Semua Tingkat</p>
                </div>
                <div className="course-author d-flex align-items-center">
                  <img
                    src="/images/user1.jpg"
                    className="rounded-circle"
                    alt="image"
                  />
                  <span>Chef Iwan</span>
                </div>

                <h3>
                  <Link href="/#">
                    <a>100+ Resep Minuman Kekinian</a>
                  </Link>
                </h3>

                <p>
                  Peserta akan mempelajari cara membuat minuman kekinian dan
                  mengkreasikannya sehingga mampu untuk membuat inovasi baru
                  dari resep yang sudah ada
                </p>

                {/* TODO: Add star rating component */}

                {/* <h6>
                  <b>Rp10.000</b>
                </h6> */}
              </div>
            </div>
          </div>

          {/* <div className="col-lg-12 col-md-12">
            <div className="courses-info">
              <p>
                Tidak menemukan yang anda cari?{" "}
                <Link href="/profile-authentication">
                  <a>Lihat semua kelas</a>
                </Link>
              </p>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default PopularCourses;
