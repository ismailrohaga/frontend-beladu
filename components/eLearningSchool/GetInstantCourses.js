import React from "react";
import Link from "next/link";

const GetInstantCourses = () => {
  return (
    <div className="get-instant-courses-area">
      <div className="container">
        <div className="get-instant-courses-inner-area">
          <div className="row align-items-center">
            <div className="col-lg-8 col-md-12">
              <div className="get-instant-courses-content">
                <h2>Konsultasi dengan Docpreneur</h2>
                <p>
                  Docpreneur merupakan fitur utama kami dimana para
                  UMKM/pengusaha akan kami diagnosa agar dapat menentukan
                  masalah yang sedang mereka alami lalu akan dilanjutkan dengan
                  konsultasi. Dengan Docpreneur UMKM/Pengusaha juga akan
                  diberikan rekomendasi untuk menerima jasa, commerce, atau
                  bimbingan dari expert yang ada di BELADU.
                </p>

                <Link href="/profile-authentication">
                  <a className="default-btn">
                    Start For Free <span></span>
                  </a>
                </Link>
              </div>
            </div>

            <div className="col-lg-4 col-md-12">
              <div className="get-instant-courses-image">
                <img src="/images/consult1.png" alt="image" />
                <div className="shape7">
                  <img src="/images/shape4.png" alt="image" />
                </div>
                <div className="shape6">
                  <img src="/images/shape6.png" alt="image" />
                </div>
              </div>
            </div>
          </div>

          <div className="shape5">
            <img src="/images/shape5.png" alt="image" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GetInstantCourses;
