import React from "react";
import Link from "next/link";

const Features = () => {
  return (
    <div className="features-area pt-100 pb-70">
      <div className="container">
        <div className="section-title">
          <h2>Benefit Bergabung Bersama Kami​</h2>
          <p>
            Dengan bergabung Bersama BELADU/BELADU.COM bisnis anda akan kami
            bina agar dapat untuk memiliki daya saing dengan bisnis-bisnis
            ternama di Indonesia.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-features-box">
              <div className="icon">
                <img src="/images/kelas.svg" alt="logo" />
              </div>
              <h3>Kelas Terbaik</h3>
              <p>
                BELADU/BELADU.COM menyediakan modul-modul berkelas dari delapan
                kategori expert kami.
              </p>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-features-box">
              <div className="icon">
                <img src="/images/event.svg" alt="logo" />
              </div>
              <h3>Event UMKM</h3>
              <p>
                BELADU/BELADU.COM menyediakan event-event sebagai sarana belajar
                lainnya dalam mengembangkan bisnis anda.
              </p>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-features-box">
              <div className="icon">
                <img src="/images/fasilitator.svg" alt="logo" />
              </div>
              <h3>Fasilitator Terbaik</h3>
              <p>
                Expert dan Modul yang ada di BELADU/BELADU.COM sudah
                terverifikasi dan sudah disaring melalui berbagai tahapan, untuk
                memastikan bahwa kami memiliki Expert dan Modul yang berkelas.
              </p>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-features-box">
              <div className="icon">
                <img src="/images/konsultasi.svg" alt="logo" />
              </div>
              <h3>Docpreneur</h3>
              <p>
                BELADU/BELADU.COM memiliki jasa konsultasi bernama Docpreneur
                untuk menampung dan membantu menyelesaikan masalah-masalah
                mengenai binis yang sedang anda jalankan.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
