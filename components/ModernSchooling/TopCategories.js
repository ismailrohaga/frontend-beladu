import React from "react";
import Link from "next/link";

const TopCategories = () => {
  return (
    <div className="categories-area ptb-100">
      <div className="container">
        <div className="section-title">
          <h2>Explore Beladu</h2>
          <p>
            BELADU/BELADU.COM memiliki banyak modul yang terbagi kedalam
            kategori sebagai berikut.
          </p>
        </div>

        <div className="row">
          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie1.jpg" alt="image" />

              <div className="content">
                <h3>Peternakan</h3>
                {/* <span>10 Courses</span> */}
              </div>

              {/* <Link href="/courses-1"> */}
              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie2.jpg" alt="image" />

              <div className="content">
                <h3>Perkebunan</h3>
                {/* <span>20 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie3.jpg" alt="image" />

              <div className="content">
                <h3>Kuliner</h3>
                {/* <span>15 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie4.jpg" alt="image" />

              <div className="content">
                <h3>Kerajinan Tangan</h3>
                {/* <span>11 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie5.jpg" alt="image" />

              <div className="content">
                <h3>Audio & Visual</h3>
                {/* <span>10 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie6.jpg" alt="image" />

              <div className="content">
                <h3>Pemasaran</h3>
                {/* <span>12 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie7.jpg" alt="image" />

              <div className="content">
                <h3>Kampung Kreatif</h3>
                {/* <span>05 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 col-md-6">
            <div className="single-categories-box">
              <img src="/images/categories/categorie8.jpg" alt="image" />

              <div className="content">
                <h3>Musik Tradisional</h3>
                {/* <span>20 Courses</span> */}
              </div>

              <Link href="/#">
                <a className="link-btn"></a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopCategories;
