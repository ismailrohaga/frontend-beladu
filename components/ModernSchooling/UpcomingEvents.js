import React from "react";
import Link from "next/link";
import { useEvent } from "hooks";
import EventCard from "components/Card/EventCard";

const UpcomingEvents = () => {
  const { useGetEventList } = useEvent();
  const { data: results, isLoading } = useGetEventList(["getEventList"]);

  return (
    <React.Fragment>
      <div className="events-area bg-fffaf3 pt-100 pb-70">
        <div className="container">
          <div className="section-title">
            <h2>Event Mendatang</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>

          <div className="row">
            {isLoading ? (
              <h4 className="text-center">Mengambil Data Event...</h4>
            ) : results.data.length >= 3 ? (
              results.data
                .slice(0, 3)
                .map((event, key) => <EventCard key={key} {...event} />)
            ) : (
              results.data.map((event, key) => (
                <EventCard key={key} {...event} />
              ))
            )}

            <div className="col-lg-12 col-md-12 text-center">
              <div className="courses-info">
                <p>
                  Tidak menemukan yang anda cari?{" "}
                  <Link href="/events">
                    <a>Lihat semua event</a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default UpcomingEvents;
