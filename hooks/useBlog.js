import { useQuery } from "react-query";

import { blogService } from "services";

export default function useBlog() {
  return {
    useGetBlogList: (key, params, queryOptions) => {
      return useQuery(key, () => blogService.getBlogs(params), {
        ...queryOptions,
        select: (data) => {
          return data.results;
        },
      });
    },
    useGetBlog: (
      key,
      referenceId,
      config = { queryOptions: {}, params: {} }
    ) => {
      const { params, queryOptions } = config;

      return useQuery(key, () => blogService.getBlog(referenceId, params), {
        ...queryOptions,
        select: (data) => data.results,
        enabled: !!referenceId,
      });
    },
  };
}
