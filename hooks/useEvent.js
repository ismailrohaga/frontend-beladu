import { useQuery, useMutation } from "react-query";

import { eventService } from "services";

export default function useEvent() {
    return {
        useGetEventList: (key, params, queryOptions) => {
            return useQuery(key, () => eventService.getEvents(params), {
                ...queryOptions,
                select: (data) => {
                    return data.results;
                },
            });
        },
        useGetEvent: (
            key,
            referenceId,
            config = { queryOptions: {}, params: {} }
        ) => {
            const { params, queryOptions } = config;

            return useQuery(
                key,
                () => eventService.getEvent(referenceId, params),
                {
                    ...queryOptions,
                    select: (data) => data.results,
                    enabled: !!referenceId,
                }
            );
        },
        useBookingEvent: () => {
            return useMutation((payload) => eventService.bookingEvent(payload));
        },
    };
}
