import React from "react";
import Navbar from "../components/_App/Navbar";
import PageBanner from "../components/Common/PageBanner";
import ContactForm from "../components/Contact/ContactForm";
import Footer from "../components/_App/Footer";
import GoogleMap from "../components/Contact/GoogleMap";

const Contact = () => {
  return (
    <React.Fragment>
      <Navbar />
      <PageBanner
        pageTitle="Contact"
        homePageUrl="/"
        homePageText="Home"
        activePageText="Contact"
      />

      <div className="contact-area ptb-100">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="contact-info">
                <span className="sub-title">Detail Kontak</span>
                <h2>Get in Touch</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Quis ipsum suspendisse ultrices gravida. Risus commodo
                  viverra.
                </p>

                <ul>
                  <li>
                    <div className="icon">
                      <i className="bx bx-map"></i>
                    </div>
                    <h3>Alamat Kami</h3>
                    <p>
                      Jl. Tubagus Ismail Raya No.17, Sekeloa, Kecamatan Coblong,
                      Kota Bandung, Jawa Barat 40134
                    </p>
                  </li>
                  <li>
                    <div className="icon">
                      <i className="bx bx-phone-call"></i>
                    </div>
                    <h3>Contact</h3>
                    <p>
                      Mobile: <a href="tel:+6285722681218">(+62) 85722681218</a>
                    </p>
                    <p>
                      Mail: <a href="mailto:info@beladu.com">info@beladu.com</a>
                    </p>
                  </li>
                  <li>
                    <div className="icon">
                      <i className="bx bx-time-five"></i>
                    </div>
                    <h3>Jam Kerja</h3>
                    <p>Monday - Friday: 09:00 - 17:00</p>
                    <p>Saturday & Sunday: 10:30 - 14:00</p>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <ContactForm />
            </div>
          </div>
        </div>
      </div>

      <GoogleMap />

      <Footer />
    </React.Fragment>
  );
};

export default Contact;
