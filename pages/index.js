import React from "react";
import Navbar from "../components/_App/Navbar";
import MainBanner from "../components/VendorCertificationTraining/MainBanner";
import Partner from "../components/eLearningSchool/Partner";
import Features from "../components/eLearningSchool/Features";
import AboutUs from "../components/ModernSchooling/AboutUs";
import PopularCourses from "../components/eLearningSchool/PopularCourses";
import GetInstantCourses from "../components/eLearningSchool/GetInstantCourses";
import LatestNews from "../components/Common/LatestNews";
import Footer from "../components/_App/Footer";
import AboutArea from "../components/VendorCertificationTraining/AboutArea";
import TopCategories from "../components/ModernSchooling/TopCategories";
import DistanceLearning from "../components/ModernSchooling/DistanceLearning";
import UpcomingEvents from "../components/ModernSchooling/UpcomingEvents";

const Index = () => {
  return (
    <React.Fragment>
      <Navbar />
      <MainBanner />
      <Features />
      <UpcomingEvents />
      <PopularCourses />
      <TopCategories />

      {/* bridge to docpreneur */}
      <GetInstantCourses />

      <AboutArea />
      <LatestNews />
      {/* <DistanceLearning /> */}
      <Footer />
    </React.Fragment>
  );
};

export default Index;
