import React from "react";
import EventSpeakers from "components/SingleEvents/EventParticipants";
import EventsSidebar from "components/SingleEvents/EventsSidebar";
import EventsTimer from "components/SingleEvents/EventsTimer";
import Footer from "components/_App/Footer";
import Navbar from "components/_App/Navbar";
import PageBanner from "components/SingleCourses/PageBanner";
import { generateIndonesianDate, generateIndonesianTime } from "utils/intl";

import { useRouter } from "next/router";
import { useEvent } from "hooks";

const EventDetail = () => {
  const router = useRouter();
  const { useGetEvent } = useEvent();

  const { data: results, isLoading } = useGetEvent(
    ["getEvent"],
    router.query?.id
  );

  let locationEmbedSrc =
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d990.2574476612807!2d107.62280072919314!3d-6.88703509968895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7b8f8a93173%3A0xff0289297e49f34c!2sBeladu.com%20-%20Belajar%20Langsung%20Dapat%20Uang!5e0!3m2!1sen!2sid!4v1657779581951!5m2!1sen!2sid";

  if (results?.locationEmbedSrc !== null) {
    locationEmbedSrc = results?.locationEmbedSrc;
  }

  if (isLoading) {
    return (
      <div className="d-flex vh-100 justify-content-center align-items-center">
        <h4>Memuat Halaman...</h4>;
      </div>
    );
  }

  return (
    <React.Fragment>
      <Navbar />
      <PageBanner
        pageTitle={results?.name}
        homePageUrl="/"
        homePageText="Home"
        innerPageUrl="/events"
        innerPageText="Events"
        activePageText={results?.name}
      />

      <div className="events-details-area pb-100">
        <div className="events-details-image bg-black">
          <img
            src={results?.coverImageUrl}
            style={{
              width: "100%",
              height: "400px",
              objectFit: "cover",
            }}
          />

          <EventsTimer endTime={new Date(results?.dateTime)} />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-lg-8 col-md-12">
              <div className="events-details-header">
                <ul>
                  <li>
                    <i className="bx bx-calendar"></i>{" "}
                    {generateIndonesianDate(results?.dateTime)}
                  </li>
                  <li>
                    <i className="bx bx-map"></i>
                    {results?.location}
                  </li>
                  <li>
                    <i className="bx bx-time"></i>
                    {generateIndonesianTime(results?.dateTime)}
                  </li>
                </ul>
              </div>

              <div className="events-details-location">
                <div id="map">
                  <iframe src={locationEmbedSrc}></iframe>
                </div>
              </div>

              <div className="events-details-desc">
                <h3>Tentang Event</h3>
                <p>{results?.content}</p>
              </div>
            </div>

            <div className="col-lg-4 col-md-12">
              <EventsSidebar data={results} />
            </div>
          </div>
        </div>
      </div>

      <EventSpeakers speakers={results?.speakers} />

      <Footer />
    </React.Fragment>
  );
};

export default EventDetail;
