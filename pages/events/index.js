import React from "react";
import Navbar from "../../components/_App/Navbar";
import PageBanner from "../../components/Common/PageBanner";
import Footer from "../../components/_App/Footer";
import SubscribeForm from "../../components/Common/SubscribeForm";
import { useEvent } from "hooks";
import EventCard from "components/Card/EventCard";

const Events = () => {
    const { useGetEventList } = useEvent();
    const { data: results, isLoading } = useGetEventList(["getEventList"]);

    return (
        <React.Fragment>
            <Navbar />
            <PageBanner
                pageTitle="Events"
                homePageUrl="/"
                homePageText="Home"
                activePageText="Events"
            />

            <div className="events-area pt-100 pb-70">
                <div className="container">
                    <div className="shorting">
                        <div className="row">
                            {isLoading ? (
                                <h4 className="text-center">
                                    Mengambil Data Event...
                                </h4>
                            ) : (
                                results.data.map((event, key) => (
                                    <EventCard key={key} {...event} />
                                ))
                            )}
                            <div className="col-lg-4 col-sm-6 col-md-6"></div>
                        </div>
                    </div>
                </div>
            </div>

            <SubscribeForm />
            <Footer />
        </React.Fragment>
    );
};

export default Events;
