import RequestAdapterService from "./requestAdapter.service";

class EventService extends RequestAdapterService {
  async getEvents(params) {
    try {
      const { data } = await this.sendGetRequest(`/client/events`, params);
      return data;
    } catch (error) {
      throw error;
    }
  }

  async getEvent(referenceId, params) {
    try {
      const { data } = await this.sendGetRequest(
        `/client/events/${referenceId}`,
        params
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async bookingEvent(body) {
    try {
      const { data } = await this.sendPostRequest(
        `/client/events/register`,
        body
      );
      return data;
    } catch (error) {
      throw error;
    }
  }
}

export default new EventService();
