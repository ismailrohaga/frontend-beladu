import axios from "axios";

class RequestAdapterService {
  constructor(baseURL = process.env.NEXT_PUBLIC_BASE_URL) {
    let requestHeader = {
      "Content-Type": "application/json",
    };

    this.reqClient = axios.create({
      headers: requestHeader,
      baseURL,
    });

    this.reqClient.interceptors.request.use(this.interceptRequestToken);

    this.reqClient.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        if (error.response) {
          if (error.response.status === 401 || error.response.status === 403) {
            localStorage.clear();
            window.location.reload();
          }

          throw error.response.data?.results;
        }

        throw error;
      }
    );
  }

  interceptRequestToken(config) {
    const auth = localStorage.getItem("auth");
    if (auth !== null) {
      const { token } = JSON.parse(auth);
      config.headers["Authorization"] = `Bearer ${token}`;
    }

    return config;
  }

  overrideAuthToken(authToken) {
    this.reqClient.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${authToken}`;
    return true;
  }

  async sendGetRequest(URL, params) {
    try {
      const response = await this.reqClient.get(URL, { params });

      return response;
    } catch (error) {
      throw error;
    }
  }

  async sendPostRequest(URL, requestBody, params) {
    try {
      const qparams = params && { params };
      const response = await this.reqClient.post(URL, requestBody, qparams);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async sendPutRequest(URL, requestBody) {
    try {
      const response = await this.reqClient.put(URL, requestBody);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async sendPatchRequest(URL, requestBody) {
    try {
      const response = await this.reqClient.patch(URL, requestBody);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async sendDeleteRequest(URL, requestBody) {
    try {
      const response = await this.reqClient.delete(URL, requestBody);
      return response;
    } catch (error) {
      throw error;
    }
  }

  static parseQuery(queryString) {
    const query = {};
    const pairs = (
      queryString[0] === "?" ? queryString.substr(1) : queryString
    ).split("&");
    for (let i = 0; i < pairs.length; i++) {
      const pair = pairs[i].split("=");
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
    }
    return query;
  }

  parseToBase64(data, mimeType) {
    const base64Str = Buffer.from(data, "binary").toString("base64");
    return "data:" + mimeType + ";base64," + base64Str;
  }
}

export default RequestAdapterService;
