import RequestAdapterService from "./requestAdapter.service";

class BlogService extends RequestAdapterService {
  async getBlogs(params) {
    try {
      const { data } = await this.sendGetRequest(`/client/blog`, params);
      return data;
    } catch (error) {
      throw error;
    }
  }

  async getBlog(referenceId, params) {
    try {
      const { data } = await this.sendGetRequest(
        `/client/blog/${referenceId}`,
        params
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async commentBlog(body) {
    try {
      const { data } = await this.sendPostRequest(`/client/blog/comment`, body);
      return data;
    } catch (error) {
      throw error;
    }
  }

  async editCommentBlog(referenceId, body) {
    try {
      const { data } = await this.sendPatchRequest(
        `/client/blog/comment/${referenceId}`,
        body
      );
      return data;
    } catch (error) {
      throw error;
    }
  }

  async deleteCommentBlog(referenceId, body) {
    try {
      const { data } = await this.sendDeleteRequest(
        `/client/blog/comment/${referenceId}`,
        body
      );
      return data;
    } catch (error) {
      throw error;
    }
  }
}

export default new BlogService();
