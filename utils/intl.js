export const generateIndonesianDate = (date, options) => {
  const dateTime = new Date(date);

  if (dateTime.toString() === "Invalid Date") {
    return "";
  }

  return new Intl.DateTimeFormat("id-ID", {
    dateStyle: "medium",
    ...options,
  }).format(dateTime);
};

export const generateIndonesianTime = (date, options) => {
  const dateTime = new Date(date);

  if (dateTime.toString() === "Invalid Date") {
    return "";
  }

  return new Intl.DateTimeFormat("id-ID", {
    timeStyle: "short",
    ...options,
  }).format(dateTime);
};

export const generateIDR = (number) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    currencyDisplay: "symbol",
  }).format(number);
};
